/*
 * Copyright 2019, the jbotsim-ghs contributors
 *
 *
 * This file is part of jbotsim-ghs.
 *
 * jbotsim-ghs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jbotsim-ghs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with jbotsim-ghs.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ghs;

import ghs.ghsutil.GHSMessageTracker;
import ghs.ghsutil.GHSUtil;
import ghs.util.Log;
import io.jbotsim.core.Color;
import io.jbotsim.core.Link;
import io.jbotsim.core.Message;
import io.jbotsim.core.Node;

import java.util.HashSet;
import java.util.Set;

public class GHSNode extends Node
{

	private static double scale = 1;

	/**
	 * State of the algorithm.
	 */
	public enum State {
		/**
		 * Initial state of the node.
		 */
		INIT,
		/**
		 * The node is waiting for the PULSE message, which will come from the root.
		 * The node is in this state at the beginning of each phase, so it is either the first phase (just after INIT),
		 * or after the end of the previous phase.
		 */
		AWAIT_PULSE,
		/**
		 * The node has already sent the FRAG message to its neighbors and is waiting for their replies (ACK_FRAG), and
		 * for the OUT message from each of its children.
		 */
		AWAIT_ACK_FRAGS_AND_OUT,
		/**
		 * The node has sent its OUT message to its father. It is waiting for the message which informs that the current
		 * root of the fragment has chosen the new root of the fragment (CHOSEN_ROOT).
		 */
		AWAIT_CHOSEN_ROOT,
		/**
		 * The node knows the new root in the former fragment. It is waiting for the message which will inform it of the
		 * the new fragment ID (NEW message).
		 */
		AWAIT_NEW_MESSAGE,
		/**
		 * The node has received the NEW message. The fragment to which it belongs has thus already merged.
		 * Before being able to send its ECHO message (terminating this phase) to its father, the node waits for:
		 * <li>
		 *     <ul>the ACK_MERGE message from its neighbors in late fragments (meaning that this neighbor won't send a
		 *     MERGE message during this phase);</ul>
		 *     <ul>the ECHO message from each of its children.</ul>
		 * </li>
		 * Note that receiving a MERGE message in this state, will trigger a NEW message (not changing the state of the
		 * node) to inform the late fragment of the new fragment ID.
		 */
		AWAIT_ECHOS_AND_ACK_MERGES,
		/**
		 * The node has received a STOP message, informing it of the end of the algorithm. This end has been detected
		 * by the root of the final fragment.
		 */
		FINISHED
	}

	protected static final String FLAG_ACK_FRAG = "ACK_FRAG";
	protected static final String FLAG_FRAG = "FRAG";
	protected static final String FLAG_PULSE = "PULSE";
	protected static final String FLAG_OUT = "OUT";
	protected static final String FLAG_CHOSEN_ROOT = "CHOSEN_ROOT";
	protected static final String FLAG_MERGE = "MERGE";
	protected static final String FLAG_NEW = "NEW";
	protected static final String FLAG_ACK_MERGE = "ACK_MERGE";
	protected static final String FLAG_ECHO = "ECHO";
	protected static final String FLAG_STOP = "STOP";

	protected static final int LINK_WIDTH_DEFAULT = (int) (Link.DEFAULT_WIDTH + 6 * scale);
	protected static final int LINK_WIDTH_SELECTED_OUT = (int) (Link.DEFAULT_WIDTH + 12 * scale);

	protected static final int NODE_ICON_SIZE_ROOT = (int) (Node.DEFAULT_ICON_SIZE * scale / 1.5);
	protected static final int NODE_ICON_SIZE_DEFAULT = (int) (Node.DEFAULT_ICON_SIZE * scale * 1);

	private int phase;
	public int frag;

	private Node father;
	private Set<Node> children;
	private Link fragBorderOutLink;
	private Node nextNodeToBorderOutLink;


	private GHSMessageTracker tracker;

	public State algoState;

	public GHSNode()
	{
		super();

		children = new HashSet<>();

		tracker = new GHSMessageTracker();
	}
	
	private void resetAlgo()
	{
		setWirelessStatus(false);
		disableWireless();

		resetLinksUI();

		setFather(null);
		setFrag(getID());

		resetOnNewPhase(0);

		children.clear();
		tracker.clear();

		setAlgoState(State.INIT);

	}

	private void resetOnNewPhase(int newPhase)
	{
		setPhase(newPhase);

		fragBorderOutLink = null;
		nextNodeToBorderOutLink = null;

		updateLabel();
	}
	
	private void startPULSEWave() {
		forwardPULSEWave();
	}

	private void sendToChildren(Object content, String flag) {
		for (Node n : children)
			send(n, new Message(content, flag));
	}

	private void sendToNeighbors(Object content, String flag) {
		for (Node n : getNeighbors())
			send(n, new Message(content, flag));
	}

	@Override
	public void onStart()
	{
		super.onStart();

		resetAlgo();
		startPULSEWave();

		if(Main.simu.isLog() && getID() == 0)
			Log.log("----------------START ALGORITHM----------------",false);

	}

	@Override
	public void onClock()
	{
		sendAckFragIfNeeded();
		actIfAllAckFragMessagesAndOutMessagesReceived();
		checkAckMergeEchoMessage();
	}

	private void sendAckFragIfNeeded()
	{
		if(!tracker.hasFRAGs())
			return;

		for(GHSMessageTracker.FRAGEntry entry : tracker.getFRAGsEntries())
			if( phase >= entry.phaseNumber) {
				send(tracker.getFRAGSender(entry), new Message(frag, FLAG_ACK_FRAG));
				tracker.removeFRAG(entry);
			}
	}

	private void actIfAllAckFragMessagesAndOutMessagesReceived()
	{
		if(!allAckFragMessagesReceived() || !allOutMessagesReceived())
			return;

		tracker.resetACK_FRAG();
		tracker.resetOUT();

		if(isRoot())
			if(fragBorderOutLink == null) // There has been no new possible merge during the phase
				sendTerminationWave();
			else
				confirmNewRootAsMySuccessor();
		else
			forwardFragBorderToFather();

	}

	private void checkAckMergeEchoMessage()	{
		crashIfInconsistentState();

		if(algoState == State.AWAIT_ECHOS_AND_ACK_MERGES
				&& allAckMergeMessagesReceived() && allEchoMessagesReceived()) {
			tracker.resetECHO();

			if(isRoot())
				startPULSEWave();
			else {
				send(father, new Message(null, FLAG_ECHO));
				setAlgoState(State.AWAIT_PULSE);
			}

		}

	}



	@Override
	public void onMessage(Message mess)
	{
		log("NODE:" + this + " with " + children.size() + " children RECEIVE: " + mess.getFlag() + " from: " + mess.getSender());
		switch(mess.getFlag())
		{
			case FLAG_PULSE:
				onPULSE();
				break;
			case FLAG_FRAG:
				onFRAG(mess);
				break;
			case FLAG_ACK_FRAG:
				onACK_FRAG(mess);
				break;
			case FLAG_OUT:
				onOUT(mess);
				break;
			case FLAG_CHOSEN_ROOT:
				onCHOSEN_ROOT(mess);
				break;
			case FLAG_MERGE:
				onMERGE(mess);
				break;
			case FLAG_NEW:
				onNEW(mess);
				break;
			case FLAG_ACK_MERGE:
				onACK_MERGE(mess);
				break;
			case FLAG_ECHO:
				onECHO(mess);
				break;
			case FLAG_STOP:
				onSTOP(mess);
				break;
		}
	}

	private void onSTOP(Message mess) {
		// propagate "STOP" message to have a uniform state on the nodes
		setAlgoState(State.FINISHED);
		updateParentEdgeUIOnSTOP();
		sendToChildren(null, FLAG_STOP);
	}

	private void onECHO(Message mess) {

		checkMessageFlag(FLAG_ECHO, mess);

		tracker.incrementECHOs();;
		log(tracker.getECHOsValue() + " on " + children.size());
	}

	private void onACK_MERGE(Message mess) {

		checkMessageFlag(FLAG_ACK_MERGE, mess);

		storeAckMerge((int) mess.getContent());
	}

	private void storeAckMerge(int phaseNumber) {
		tracker.incrementACK_MERGEs(phaseNumber);

		log(tracker.getACK_MERGEsValue(phaseNumber) + " on " + getNeighbors().size() + " for phase:" + phaseNumber);
	}

	private void onNEW(Message mess) {

		checkMessageFlag(FLAG_NEW, mess);

		applyNewFragmentConfigurationOnNEW(mess);

		updateParentEdgeUIOnNEW();
	}

	private void applyNewFragmentConfigurationOnNEW(Message mess) {
		Node sender = mess.getSender();
		int newFrag = (Integer) mess.getContent();

		setFather(sender);
		children.remove(father);

		setFrag(newFrag);
		forwardNEWWave();
	}

	private void onMERGE(Message mess) {

		checkMessageFlag(FLAG_MERGE, mess);

		Node sender = mess.getSender();
		int otherFragment = (Integer) mess.getContent();

		switch (algoState){
			case AWAIT_NEW_MESSAGE:
				startNEWWaveIfRootOfWinningFragment(sender, otherFragment);
				break;
			case AWAIT_ECHOS_AND_ACK_MERGES:
				informLateMergerWithNEW(sender);
				break;

			default:
				// we are ready to merge yet (fragment's best out not received yet). Store merge request.
				tracker.addMERGE(new GHSMessageTracker.MERGEEntry(sender, otherFragment));
		}

	}

	private void informLateMergerWithNEW(Node sender) {
		// I have already merged (I know my new fragment)
		// Sender is late for its MERGE.
		// I inform it of my new fragment number anyway (it will choose it).
		children.add(sender);
		send(sender,new Message(frag, FLAG_NEW));
	}

	private void startNEWWaveIfRootOfWinningFragment(Node sender, int otherFragment) {
		// I already know the new root of my fragment.
		// I receive a MERGE from the root of another fragment.
		// If I am the new root of my fragment and the MERGE comes from my fragment's new outLink and my former
		// fragment "wins" the merge, then I should start the NEW phase to inform everyone
		// (including the newly merged fragment) of the new fragment number.

		children.add(sender);//adding the other node to children

		boolean isChosenByOtherFragmentRoot = getCommonLinkWith(sender) == fragBorderOutLink;
		boolean isWinningFragment = otherFragment > frag;

		if(isRoot() && isChosenByOtherFragmentRoot && isWinningFragment) {
			updateChildrenOnMerge();
			tracker.clearMERGEs();
			forwardNEWWave();
			log("---------MERGING FRAG: " + frag + " AND " + otherFragment + "-----------");
		}
	}

	private void onCHOSEN_ROOT(Message mess) {
		checkMessageFlag(FLAG_CHOSEN_ROOT, mess);

		handleNewRoot(mess.getSender(), (Link) mess.getContent());
	}

	private void sendMERGEIfNewFragRoot(Link outLink) {
		if (nextNodeToBorderOutLink == null && fragBorderOutLink == outLink)
			// you are the new root of the fragment
			send(fragBorderOutLink.getOtherEndpoint(this), new Message(frag, FLAG_MERGE));
	}

	private void flipPathToFragBorderIfNeeded(Link outLink, Node formerFather) {

		if(!isOnNewRootPath(outLink))
			return;

		if(formerFather != null)
			children.add(formerFather);

		setFather(nextNodeToBorderOutLink);
	}

	private boolean isOnNewRootPath(Link outLink) {
		// issue #65 created on jbotsim for NPE
		return fragBorderOutLink != null && outLink == fragBorderOutLink;
	}

	private void checkMessageFlag(String expectedFlag, Message mess) {
		if (!expectedFlag.equals(mess.getFlag()))
			throw new IllegalArgumentException();
	}

	private void onOUT(Message mess) {
		tracker.incrementOUTs();
		log(tracker.getOUTsValue() + " on " + children.size());

		updateBorderPathOnBetterChildProposal(mess);
	}

	private void updateBorderPathOnBetterChildProposal(Message mess) {
		if(mess.getContent() == null)
			return;

		Link outgoingEdge = (Link) mess.getContent();
		if(GHSUtil.isSmaller(outgoingEdge, fragBorderOutLink)) {
			fragBorderOutLink = outgoingEdge;
			nextNodeToBorderOutLink = mess.getSender();
		}
	}

	private void onACK_FRAG(Message mess) {
		tracker.incrementACK_FRAGs();
		log(tracker.getACK_FRAGsValue() + " on " + getNeighbors().size());
		updateBorderOutLinkIfBetterOutGoingEdge(mess);
	}

	private void updateBorderOutLinkIfBetterOutGoingEdge(Message mess) {
		int senderFragNumber = (Integer) mess.getContent();
		if(senderFragNumber == frag)
			return;

		Link outgoingEdge = getCommonLinkWith(mess.getSender());
		if(GHSUtil.isSmaller(outgoingEdge, fragBorderOutLink)) {
			fragBorderOutLink = outgoingEdge;
			nextNodeToBorderOutLink = null;
		}
	}

	private void onFRAG(Message mess) {
		replyOrStoreFRAG(mess);
	}

	private void replyOrStoreFRAG(Message mess) {
		Integer phaseNumber = (Integer) mess.getContent();
		Node sender = mess.getSender();

		if(phase >= phaseNumber)
			send(sender, new Message(frag, FLAG_ACK_FRAG)); //answer it
		else
			tracker.putFRAG(new GHSMessageTracker.FRAGEntry(sender.getID(), phaseNumber), sender);
	}

	private void onPULSE() {

		forwardPULSEWave();
	}


	@Override
	public void send(Node n, Message mess) {
		super.send(n, mess);
		log("NODE:" + this + " with " + children.size() + " children SEND: " + mess.getFlag() + " to: " + n);
	}


	private void forwardFragBorderToFather() {
		send(father,new Message(fragBorderOutLink, FLAG_OUT));
		setAlgoState(State.AWAIT_CHOSEN_ROOT);
	}

	private void confirmNewRootAsMySuccessor() {
		fragBorderOutLink.setWidth(LINK_WIDTH_SELECTED_OUT);
		fragBorderOutLink.setColor(Color.RED);

		handleNewRoot(null, fragBorderOutLink);
	}

	private void handleNewRoot(Node formerFather, Link outLink) {
		sendToChildren(outLink, FLAG_CHOSEN_ROOT);

		flipPathToFragBorderIfNeeded(outLink, formerFather);

		sendMERGEIfNewFragRoot(outLink);
		setAlgoState(State.AWAIT_NEW_MESSAGE);

		sendNEWWaveIfRootOfWinningFragment();

	}

	private void sendTerminationWave() {
		//end of the algorithm
		sendToChildren(null, FLAG_STOP);
		setAlgoState(State.FINISHED);
		if(Main.simu.isVerbose())
			System.out.println("GHS algorithm terminated in " + phase + " phases");
	}

	// region Convenience methods

	private boolean allOutMessagesReceived() {
		return tracker.getOUTsValue() == children.size();
	}

	private boolean allAckFragMessagesReceived() {
		return tracker.getACK_FRAGsValue() == getNeighbors().size();
	}

	private boolean allEchoMessagesReceived() {
		return tracker.getECHOsValue() == children.size();
	}

	private boolean allAckMergeMessagesReceived() {
		return tracker.getACK_MERGEsValue(phase) == getNeighbors().size();
	}

	// endregion

	private void forwardPULSEWave() {

		resetOnNewPhase(phase + 1);
		sendToChildren(null, FLAG_PULSE);
		sendToNeighbors(phase, FLAG_FRAG);

		setAlgoState(State.AWAIT_ACK_FRAGS_AND_OUT);
	}

	private void crashIfInconsistentState() {
		boolean tooMuchAcksForOnePhase = tracker.getACK_MERGEsValue(phase) > getNeighbors().size();
		boolean tooMuchEchos = tracker.getECHOsValue() > children.size();

		if(tooMuchAcksForOnePhase || tooMuchEchos)
			throw new IllegalStateException();
	}


	private void sendNEWWaveIfRootOfWinningFragment() {
		if(!tracker.hasMERGEs())
			return;

		updateChildrenOnMerge();
		if(isRoot() && isMergedFragmentNewRoot())
			forwardNEWWave();

		tracker.clearMERGEs();
	}

	private void forwardNEWWave() {
		sendToChildren(frag, FLAG_NEW);
		setAlgoState(State.AWAIT_ECHOS_AND_ACK_MERGES);
		sendToNeighbors(phase, FLAG_ACK_MERGE);
	}

	private void setAlgoState(State state) {
		algoState = state;
		updateNodeUIColor(state);
	}

	private void updateChildrenOnMerge() {
		for(GHSMessageTracker.MERGEEntry p : tracker.getMERGEsEntries())
			children.add(p.node);
	}

	private boolean isMergedFragmentNewRoot() {
		for(GHSMessageTracker.MERGEEntry p : tracker.getMERGEsEntries())
			if(fragBorderOutLink.equals(getCommonLinkWith(p.node)))
				if(frag < p.fragNumber)
					return true;
		return false;
	}

	private void log(String s)
	{
		if(Main.simu.isLog())
			Log.log(s,true);

	}

	private void updateLabel() {
		String label = getID() + ":";
		if(isRoot())
			label += "root";
		else
			label += father.getID();

		label += ":f" + frag + "::" + phase;

		setLabel(label);
	}

	private void setFather(Node father) {
		this.father = father;
		updateNodeUIForRoot();

		updateLabel();
	}

	private boolean isRoot() {
		return this.father == null;
	}

	private void setFrag(int frag) {
		this.frag = frag;

		updateLabel();
	}


	private void setPhase(int phase) {
		this.phase = phase;

		updateLabel();
	}

	// region UI methods

	private void updateParentEdgeUIOnNEW() {
		getCommonLinkWith(father).setColor(Color.getColorAt(frag));
		getCommonLinkWith(father).setWidth(LINK_WIDTH_DEFAULT);
	}
	private void updateParentEdgeUIOnSTOP() {
		getCommonLinkWith(father).setColor(new Color(75, 75, 75));
	}

	private void updateNodeUIColor(State state) {
		Color color = null;
		switch (state) {

			case INIT:
				color = Color.WHITE ; break;
			case AWAIT_PULSE:
				color = Color.RED.darker().darker() ; break;
			case AWAIT_ACK_FRAGS_AND_OUT:
				color = Color.CYAN ; break;
			case AWAIT_CHOSEN_ROOT:
				color = Color.CYAN.darker().darker() ; break;
			case AWAIT_NEW_MESSAGE:
				color = Color.PINK ; break;
			case AWAIT_ECHOS_AND_ACK_MERGES:
				color = Color.RED.brighter().brighter() ; break;
			case FINISHED:
				color = Color.BLACK ; break;
		}
		setColor(color);
	}


	private void updateNodeUIForRoot() {
		if(isRoot())
			setIconSize(NODE_ICON_SIZE_DEFAULT);
		else
			setIconSize(NODE_ICON_SIZE_ROOT);
	}

	private void resetLinksUI() {
		for(Link l: getLinks()) {
			l.setWidth(Link.DEFAULT_WIDTH);
			l.setColor(Link.DEFAULT_COLOR);
		}
	}
	// endregion
}
