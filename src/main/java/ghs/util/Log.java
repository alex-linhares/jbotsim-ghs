/*
 * Copyright 2019, the jbotsim-ghs contributors
 *
 *
 * This file is part of jbotsim-ghs.
 *
 * jbotsim-ghs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jbotsim-ghs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with jbotsim-ghs.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ghs.util;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Simple Log class.
 */
public class Log {
    public static void log(String s, boolean append)
    {
        Logger logger = Logger.getLogger("log");
        logger.setUseParentHandlers(false);

        FileHandler fh;
        boolean succeed = false;

        while(!succeed)
        {
            try {

                // This block configure the logger with handler and formatter
                fh = new FileHandler("./log.txt",append);
                logger.addHandler(fh);
                fh.setFormatter(new SimpleFormatter() {
                    private static final String format = "%1$s %n";

                    @Override
                    public synchronized String format(LogRecord lr) {
                        return String.format(format,
                                lr.getMessage()
                        );
                    }
                });

                // the following statement is used to log any messages
                logger.info(s);
                logger.removeHandler(fh);
                fh.close();
                succeed = true;

            } catch (SecurityException e) {
               // e.printStackTrace();
            } catch (IOException e) {
               // e.printStackTrace();
            }

        }
    }
}
