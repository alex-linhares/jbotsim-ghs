/*
 * Copyright 2019, the jbotsim-ghs contributors
 *
 *
 * This file is part of jbotsim-ghs.
 *
 * jbotsim-ghs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jbotsim-ghs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with jbotsim-ghs.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ghs.ghsutil;

import io.jbotsim.core.Link;

public class GHSUtil {
    /**
     * Compares two {@link Link Links} with respect to GHS specifications (lexicographical order).
     *
     * @param l1 a {@link Link}
     * @param l2 a {@link Link}
     * @return true if l1 is deemed smaller than l2 with respect to GHS's order.
     */
    public static boolean isSmaller(Link l1, Link l2)
    {
        if(l2 == null)
            return true;

        if(l1.getLength() < l2.getLength()) // distance check
            return true;

        if(l1.getLength() > l2.getLength()) // distance check
            return false;

        //if length are equal: checking ids of the links
        // Note: probably never called because lengths are on floats
        return isSmallerOnIdsOnly(l1, l2);
    }

    /**
     * Compares two {@link Link Links} with respect to their ids.
     *
     * @param l1 a {@link Link}
     * @param l2 a {@link Link}
     * @return true if l1 is deemed smaller than l2 with respect to GHS's order.
     */
    protected static boolean isSmallerOnIdsOnly(Link l1, Link l2) {
        int minL1,minL2,maxL1,maxL2;
        minL1 = l1.endpoint(0).getID();
        maxL1 = l1.endpoint(1).getID();
        if(maxL1 < minL1) {
            // swap
            int aux = minL1;
            minL1 = maxL1;
            maxL1 = aux;
        }

        minL2 = l2.endpoint(0).getID();
        maxL2 = l2.endpoint(1).getID();
        if(maxL2 < minL2) {
            // swap
            int aux = minL2;
            minL2 = maxL2;
            maxL2 = aux;
        }

        if(minL1 < minL2)
            return true;

        if(minL1 > minL2)
            return false;

        if(maxL1 < maxL2)
                return true;

        return false;
    }
}
