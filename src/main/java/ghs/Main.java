/*
 * Copyright 2019, the jbotsim-ghs contributors
 *
 *
 * This file is part of jbotsim-ghs.
 *
 * jbotsim-ghs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jbotsim-ghs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with jbotsim-ghs.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ghs;

public class Main{

	public static Simu simu;

	public static void main(String[] args) {

		simu = new Simu(new Simu.GHSProperty(args));
		simu.run();

	}

}
