/*
 * Copyright 2019, the jbotsim-ghs contributors
 *
 *
 * This file is part of jbotsim-ghs.
 *
 * jbotsim-ghs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jbotsim-ghs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with jbotsim-ghs.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ghs;

import io.jbotsim.core.*;
import io.jbotsim.core.event.ClockListener;
import io.jbotsim.core.event.MessageListener;
import io.jbotsim.io.format.dot.DotTopologySerializer;
import io.jbotsim.ui.JViewer;

import java.io.FileWriter;
import java.io.IOException;

/**
 * The {@link Simu} object is in charge of:
 * <ol>
 *     <li>load the topology;</li>
 *     <li>run the simulation;</li>
 *     <li>terminate the simulation and write the result.</li>
 * </ol>
 */
public class Simu implements ClockListener, MessageListener {

    GHSProperty property = null;

    int nbTickTotal = 0;
    int nbMessTotal = 0;
    int nbStoppedNode = 0;
    private Topology topology;

    public Simu(GHSProperty property) {
        this.property = property;
    }

    public void run() {

        String graphFilename = property._graphFilename;
        if(graphFilename != null)
        {

            topology = new Topology();
            topology.setWirelessStatus(false);
            topology.setTimeUnit(property._clockSpeed);
            topology.disableWireless();
            topology.setDefaultNodeModel(GHSNode.class);

            topology.addClockListener(this);
            topology.addMessageListener(this);

            loadTopology(graphFilename);

            if(property._shuffleIds)
                topology.shuffleNodeIds();

            if(property._display)
                new JViewer(topology);


            topology.start();

        }
    }

    private void loadTopology(String graphFilename) {
        String fileContent = topology.getFileManager().read(graphFilename);
        new DotTopologySerializer().importFromString(topology, fileContent);
    }

    @Override
    public void onClock() {
        nbTickTotal++;
    }

    @Override
    public void onMessage(Message mess) {
        nbMessTotal++;
        if(property._verboseMode && nbMessTotal%property._verboseStep == 0)
            System.out.println(nbMessTotal);

        finishIfStopMessage(mess);

    }

    private void finishIfStopMessage(Message mess) {
        if(mess.getFlag() == GHSNode.FLAG_STOP)
        {
            nbStoppedNode++;
            if(nbStoppedNode == (topology.getNodes().size()-1))
            {
                if(property._verboseMode)
                    System.out.println("With " + nbMessTotal + " messages sent.");
                topology.pause();

                if(property._resultFilename!=null)
                {
                    String result = "graph {\r\n" +
                            "	graph [layout=nop, splines=line, bb=\"-1524.82,-1487.86,4577.58,4034.63\"];\r\n" +
                            "	node [label=\"\", shape=point, height=0.05, width=0.05];\r\n" +
                            "	edge [len=1.00];\n";
                    for(Link l : topology.getLinks())
                    {
                        result+=l.endpoint(0).getID() + "--" + l.endpoint(1).getID();
                        if(l.getColor() == Color.MAGENTA)
                        {
                            result += "[color=\"magenta\",penwidth=\"5.0\"]";
                        }
                        result +=";\n";
                    }
                    for(Node n : topology.getNodes())
                    {
                        result += n.getID() + " [pos=\"" + n.getX() + "," + n.getY() + "\"];\n";
                    }
                    result +="}";
                    try (FileWriter file = new FileWriter(property._resultFilename))
                    {

                        file.write(result);
                        file.flush();

                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
                if(property._perfFilename != null)
                {
                    try (FileWriter file = new FileWriter(property._perfFilename,true))
                    {

                        String perf = topology.getNodes().size() + "," + topology.getLinks().size() + "," + nbMessTotal + "," + nbTickTotal + "\n";
                        file.write(perf);
                        file.flush();

                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
                //if(!property._display)
                //System.exit(0);


            }

        }
    }

    // region Getter/Setter
    public boolean isLog() {
        return property._log;
    }
    public boolean isVerbose() {
        return property._verboseMode;
    }
    // endregion

    /**
     * {@link GHSProperty} parses and holds the parameters of the program.
     */
    public static class GHSProperty
    {

        public boolean _log = false;
        public String _graphFilename = null;
        public String _resultFilename = null;
        public boolean _verboseMode = false;
        public int _verboseStep = 1;
        public int _clockSpeed = 50;
        public boolean _shuffleIds = false;
        public boolean _display = false;
        public String _perfFilename = null;

        public GHSProperty(String[] args)
        {
            parseArgs(args);
        }

        private void parseArgs(String[] args) {
            for(int i = 0;i<args.length;i++)
            {
                String arg = args[i];
                if(arg.startsWith("-"))
                {
                    switch (arg)
                    {
                    case "-log":
                        _log = true;
                        break;

                    case "-s":
                        if( i+1< args.length && !args[i+1].startsWith("-"))
                        {
                            i++;
                            _resultFilename = args[i];
                        }
                        else
                        {
                            System.out.println("Invalid filename for result.");
                        }
                        break;
                    case "-v":
                        _verboseMode = true;
                        if(i+1<args.length && !args[i+1].startsWith("-"))
                        {
                            String strVerboseStep = args[i+1];
                            try
                            {
                                _verboseStep = Integer.parseInt(strVerboseStep);
                                i++;
                            }
                            catch (NumberFormatException e)
                            {

                            }
                        }
                        break;

                    case "-cs":
                        if(i+1<args.length && !args[i+1].startsWith("-"))
                        {
                            String strVerboseStep = args[i+1];
                            try
                            {
                                _clockSpeed = Integer.parseInt(strVerboseStep);
                                i++;
                            }
                            catch (NumberFormatException e)
                            {

                            }
                        }
                        break;

                    case "-si":
                        _shuffleIds = true;
                        break;

                    case "-d":
                        _display = true;
                        break;

                    case "-p":
                        if( i+1< args.length && !args[i+1].startsWith("-"))
                        {
                            i++;
                            _perfFilename = args[i];
                        }
                        else
                        {
                            System.out.println("Invalid filename for performances.");
                        }
                        break;

                    default:
                        break;
                    }

                }
                else
                {
                    _graphFilename = arg;
                }
            }
        }

    }
}
