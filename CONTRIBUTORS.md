
## Initial authors
The jbotsim-ghs project has been initially implemented by: 

* Fabien Jacques - fabien.jacques@lirmm.fr
* Jonathan Narboni - jonathan.narboni@labri.fr

## Contributors

The JBotSim contributors are listed by alphabetical order:

* Fabien Jacques - fabien.jacques@lirmm.fr
  * initial author
* Remi Laplace - remi.laplace@labri.fr
  * project refactoring
  * visualization
* Jonathan Narboni - jonathan.narboni@labri.fr
  * initial author
  * project refactoring